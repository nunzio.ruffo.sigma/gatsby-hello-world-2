import PropTypes from "prop-types"
import React from "react"

const Card = () => {
    return (
        <div class="card mb-3">
            <div class="card-content">
                <div class="media-content">
                    <p class="title is-4">John Smith</p>
                    <p class="subtitle is-6">@johnsmith</p>
                </div>
                <div class="content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Phasellus nec iaculis mauris.
                </div>
            </div>
        </div>
    )
}

export default Card;