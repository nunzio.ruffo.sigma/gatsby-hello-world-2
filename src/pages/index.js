import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <div className="container mt-5 mb-5">
      <div className="notification content">
        <h1 className="is-large">What is the Jamstack?</h1>

        <p>You may have already seen or worked on a Jamstack site! They do not have to include all attributes of JavaScript, APIs, and Markup. They might be built using sites built by hand, or with Jekyll, Hugo, Nuxt, Next, Gatsby, or another static site generator...</p>

        <h2 className="is-medium">Why the Jamstack?</h2>
        <h4><b>Better Performance</b></h4>
        <p>Why wait for pages to build on the fly when you can generate them at deploy time? When it comes to minimizing the time to first byte, nothing beats pre-built files served over a CDN.</p>
      </div>
    </div>
  </Layout>
)

export default IndexPage
