import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import Card from "../components/card"

const ItemsPage = () => (
  <Layout>
    <SEO title="Home" />
    <div className="container mt-5 mb-5">
        <Card />
        <Card />
        <Card />
        <Card />
    </div>
  </Layout>
)

export default ItemsPage
